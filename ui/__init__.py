import libtcodpy as libtcod
import system
import ui.menu
import ui.message

SCREEN_WIDTH = 80
SCREEN_HEIGHT = 50

MAP_WIDTH = 80
MAP_HEIGHT = 43

CHARACTER_SCREEN_WIDTH = 30

BAR_WIDTH = 20
PANEL_HEIGHT = 7
PANEL_Y = ui.SCREEN_HEIGHT - PANEL_HEIGHT

MSG_X = BAR_WIDTH + 2
MSG_WIDTH = ui.SCREEN_WIDTH - BAR_WIDTH - 2
MSG_HEIGHT = PANEL_HEIGHT - 1

INVENTORY_WIDTH = 50

con = libtcod.console_new(ui.MAP_WIDTH, ui.MAP_HEIGHT)
panel = libtcod.console_new(ui.SCREEN_WIDTH, PANEL_HEIGHT)

color_dark_wall = libtcod.Color(0, 0, 100)
color_light_wall = libtcod.Color(130, 110, 50)
color_dark_ground = libtcod.Color(50, 50, 150)
color_light_ground = libtcod.Color(200, 180, 150)

def render_all():
    """Renders the main game screen"""
    if system.fov_recompute:
        system.fov_recompute = False
        libtcod.map_compute_fov(
            system.fov_map, system.player.x, system.player.y, system.TORCH_RADIUS, system.FOV_LIGHT_WALLS, system.FOV_ALGO)

    for y in range(ui.MAP_HEIGHT):
        for x in range(ui.MAP_WIDTH):
            visible = libtcod.map_is_in_fov(system.fov_map, x, y)
            wall = system.map[x][y].block_sight
            if not visible:
                if system.map[x][y].explored:
                    if wall:
                        libtcod.console_set_char_background(
                            ui.con, x, y, ui.color_dark_wall, libtcod.BKGND_SET)
                    else:
                        libtcod.console_set_char_background(
                            ui.con, x, y, ui.color_dark_ground, libtcod.BKGND_SET)
            else:
                if wall:
                    libtcod.console_set_char_background(
                        ui.con, x, y, ui.color_light_wall, libtcod.BKGND_SET)
                else:
                    libtcod.console_set_char_background(
                        ui.con, x, y, ui.color_light_ground, libtcod.BKGND_SET)
                system.map[x][y].explored = True

    for object in system.objects:
        object.draw()

    libtcod.console_blit(ui.con, 0, 0, ui.SCREEN_WIDTH,
                         ui.SCREEN_HEIGHT, 0, 0, 0)

    libtcod.console_set_default_background(ui.panel, libtcod.black)
    libtcod.console_clear(ui.panel)

    ui.render_bar(1, 1, ui.BAR_WIDTH, 'HP', system.player.fighter.hp,
               system.player.fighter.max_hp, libtcod.light_red, libtcod.darker_red)
    libtcod.console_print_ex(ui.panel, 1, 3, libtcod.BKGND_NONE,
                             libtcod.LEFT, 'Dungeon level ' + str(system.dungeon_level))

    y = 1
    for (line, color) in ui.game_msgs:
        libtcod.console_set_default_foreground(ui.panel, color)
        libtcod.console_print_ex(
            ui.panel, ui.MSG_X, y, libtcod.BKGND_NONE, libtcod.LEFT, line)
        y += 1

    libtcod.console_set_default_foreground(ui.panel, libtcod.light_grey)
    libtcod.console_print_ex(
        ui.panel, 1, 0, libtcod.BKGND_NONE, libtcod.LEFT, system.get_names_under_mouse())

    libtcod.console_blit(ui.panel, 0, 0, ui.SCREEN_WIDTH,
                         ui.PANEL_HEIGHT, 0, 0, ui.PANEL_Y)

def render_bar(x, y, total_width, name, value, maximum, bar_color, back_color):
    """Render a bar for player stats (HP, XP, etc)

    Args:
        x (int): X position to display bar
        y (int): Y position to display bar
        total_width (int): How wide to make the bar
        name (str): Label to write inside the bar
        value (int): Current value to display and calculate filled bar width
        maximum (int): Maximum value to display and use in calculation for filled bar width
        bar_color (libtcod.Color): Colour to display the filled portion of the bar
        back_color (libtcod.Color): Colour to display the unfilled portion of the bar
    """
    bar_width = int(float(value) / maximum * total_width)

    libtcod.console_set_default_background(ui.panel, back_color)
    libtcod.console_rect(ui.panel, x, y, total_width,
                         1, False, libtcod.BKGND_SCREEN)

    libtcod.console_set_default_background(ui.panel, bar_color)
    if bar_width > 0:
        libtcod.console_rect(ui.panel, x, y, bar_width, 1,
                             False, libtcod.BKGND_SCREEN)

    libtcod.console_set_default_foreground(ui.panel, libtcod.white)
    libtcod.console_print_ex(ui.panel, x + total_width / 2, y, libtcod.BKGND_NONE,
                             libtcod.CENTER, name + ': ' + str(value) + '/' + str(maximum))
