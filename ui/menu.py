import libtcodpy as libtcod
import system
import ui


def menu(header, options, width):
    """Create a menu with a `header` and selectable `options`

    Args:
        header (str): Text to display as the heading
        options (list): A list of options to display for selection
        width (int): How wide to make the menu, in characters

    Returns:
        int: The index of the selected `option`

    """
    if len(options) > 26:
        raise ValueError('Cannot have a menu with more than 26 options.')

    header_height = libtcod.console_get_height_rect(
        ui.con, 0, 0, width, ui.SCREEN_HEIGHT, header)
    if header == '':
        header_height = 0
    height = len(options) + header_height

    window = libtcod.console_new(width, height)

    libtcod.console_set_default_foreground(window, libtcod.white)
    libtcod.console_print_rect_ex(
        window, 0, 0, width, height, libtcod.BKGND_NONE, libtcod.LEFT, header)

    y = header_height
    letter_index = ord('a')
    for option_text in options:
        text = '(' + chr(letter_index) + ') ' + option_text
        libtcod.console_print_ex(
            window, 0, y, libtcod.BKGND_NONE, libtcod.LEFT, text)
        y += 1
        letter_index += 1

    x = ui.SCREEN_WIDTH / 2 - width / 2
    y = ui.SCREEN_HEIGHT / 2 - height / 2

    x_offset = x
    y_offset = y + header_height

    (menu_x, menu_y) = (system.mouse.cx - x_offset, system.mouse.cy - y_offset)
    while True:
        libtcod.console_blit(window, 0, 0, width, height, 0, x, y, 1.0, 0.7)
        libtcod.console_flush()
        libtcod.sys_check_for_event(
            libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, system.key, system.mouse)
        libtcod.console_set_default_background(window, libtcod.black)
        libtcod.console_rect(window, 0, menu_y + header_height,
                             width, 1, False, libtcod.BKGND_SET)

        (menu_x, menu_y) = (system.mouse.cx - x_offset, system.mouse.cy - y_offset)

        if menu_x >= 0 and menu_x < width and menu_y >= 0 and menu_y < height - header_height:
            libtcod.console_set_default_background(
                window, libtcod.darker_yellow)
            libtcod.console_rect(
                window, 0, menu_y + header_height, width, 1, False, libtcod.BKGND_SET)
            if (system.mouse.lbutton_pressed):
                return menu_y

        if (system.mouse.rbutton_pressed) or system.key.vk == libtcod.KEY_ESCAPE or system.key.vk == libtcod.KEY_SPACE:
            return None

        if system.key.vk == libtcod.KEY_ENTER and system.key.lalt:
            libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())

        index = system.key.c - ord('a')
        if index >= 0 and index < len(options):
            return index
        if index >= 0 and index < 26:
            return None


def inventory(header):
    """Show a menu with each item of the `inventory` as an option

    Args:
        header (str): Text to display as the heading

    Returns:
        Item: The item selected in the menu
    """
    if len(system.inventory) == 0:
        options = ['Inventory is empty.']
    else:
        options = []
        for item in system.inventory:
            text = item.name
            if item.equipment and item.equipment.is_equipped:
                text = text + ' (on ' + item.equipment.slot + ')'
            options.append(text)

    index = menu(header, options, ui.INVENTORY_WIDTH)

    if index is None or len(system.inventory) == 0:
        return None
    return system.inventory[index].item


def msgbox(text, width=50):
    """Use :py:func:`~ui.menu.menu` as a fake message box

    Args:
        text (str): Text to display in the message box
        width (int): How wide to make the message box, in characters
    """
    menu(text, [], width)
