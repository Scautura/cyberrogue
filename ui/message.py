import textwrap

import libtcodpy as libtcod
import ui


def add(new_msg, color=libtcod.white):
    """Add a message to the message queue

    Args:
        new_msg (str): The message to add to the queue
        color (libtcod.Color): Color to display message as
    """
    new_msg_lines = textwrap.wrap(new_msg, ui.MSG_WIDTH)

    for line in new_msg_lines:
        if len(ui.game_msgs) == ui.MSG_HEIGHT:
            del ui.game_msgs[0]

        ui.game_msgs.append((line, color))
