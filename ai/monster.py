import ai
import effects
import libtcodpy as libtcod
import system
import ui


class Basic:
    """Basic monster AI, follows player when he can be seen"""

    def take_turn(self):
        """Allows monster to take his turn and follow the player"""
        monster = self.owner
        if libtcod.map_is_in_fov(system.fov_map, monster.x, monster.y):
            if monster.distance_to(system.player) >= 2:
                monster.move_astar(system.player)

            elif system.player.fighter.hp > 0:
                monster.fighter.attack(system.player)


class Confused:
    """Initialise a confused monster AI so it can return to its original AI after confusion runs out

    Args:
        old_ai (class): See :py:attr:`~ai.monster.Confused.old_ai`
        num_turns (int): See :py:attr:`~ai.monster.Confused.num_turns`

    Attributes:
        old_ai (class): Original AI to return to when confusion runs out
        num_turns (int): Number of turns to allow confusion to last
    """

    def __init__(self, old_ai, num_turns=effects.magic.CONFUSE_NUM_TURNS):
        self.old_ai = old_ai
        self.num_turns = num_turns

    def take_turn(self):
        """Allows monster to take his turn and move randomly"""
        if self.num_turns > 0:
            self.owner.move(libtcod.random_get_int(0, -1, 1),
                            libtcod.random_get_int(0, -1, 1))
            self.num_turns -= 1

        else:
            self.owner.ai = self.old_ai
            ui.message.add('The ' + self.owner.name +
                           ' is no longer confused!', libtcod.red)
