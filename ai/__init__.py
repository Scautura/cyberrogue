import ai.monster
import libtcodpy as libtcod
import system
import ui


class Fighter:
    """Class for combat features of characters (both controlled by player and AI)

    Args:
        hp (int): See :py:attr:`ai.Fighter.hp` and :py:attr:`ai.Fighter.base_max_hp`
        defense (int): See :py:attr:`ai.Fighter.base_defense`
        power (int): See :py:attr:`ai.Fighter.base_power`
        xp (int): See :py:attr:`ai.Fighter.xp`
        death_function (func): See :py:attr:`ai.Fighter.death_function`

    Attributes:
        base_max_hp (int): Max HP for a character excluding bonuses
        hp (int): Current HP for a character
        base_defense (int): Defense (damage reduction) characteristic excluding bonuses
        base_power (int): Power (damage) characteristic excluding bonuses
        xp (int): Experience characteristic
        death_function (func): Function to call when the :py:class:`~ai.Fighter` dies
    """

    def __init__(self, hp, defense, power, xp, death_function=None):
        self.base_max_hp = hp
        self.hp = hp
        self.base_defense = defense
        self.base_power = power
        self.xp = xp
        self.death_function = death_function

    @property
    def power(self):
        """Current attack power including bonuses

        Returns:
            int: Current attack power including bonuses"""
        bonus = sum(
            equipment.power_bonus for equipment in system.get_all_equipped(self.owner))
        return self.base_power + bonus

    @property
    def defense(self):
        """Current defense including bonuses

        Returns:
            int: Current defense including bonuses"""
        bonus = sum(
            equipment.defense_bonus for equipment in system.get_all_equipped(self.owner))
        return self.base_defense + bonus

    @property
    def max_hp(self):
        """Current HP maximum including bonuses

        Returns:
            int: Current HP maximum including bonuses"""
        bonus = sum(
            equipment.max_hp_bonus for equipment in system.get_all_equipped(self.owner))
        return self.base_max_hp + bonus

    def take_damage(self, damage):
        """Take a given amount of damage and check to see if character has died (if so, call the :py:attr:`~ai.Fighter.death_function`)

        Args:
            damage (int): Amount of damage to cause to character"""
        if damage > 0:
            self.hp -= damage

        if self.hp <= 0:
            function = self.death_function
            if function is not None:
                function(self.owner)
            if self.owner != system.player:
                system.player.fighter.xp += self.xp

    def attack(self, target):
        """Attack the `target` and cause damage if the :py:attr:`~ai.Fighter.power` is greater than the :py:attr:`~ai.Fighter.defense`

        Args:
            target: Target to cause damage to"""
        damage = self.power - target.fighter.defense

        if damage > 0:
            ui.message.add(self.owner.name.capitalize() + ' attacks ' +
                           target.name + ' for ' + str(damage) + ' hit points.')
            target.fighter.take_damage(damage)
        else:
            ui.message.add(self.owner.name.capitalize() +
                           ' attacks ' + target.name + ' but it has no effect!')

    def heal(self, amount):
        """Heal a given `amount`

        Args:
            amount(int): Amount to heal and add to :py:attr:`~ai.Fighter.hp`"""
        self.hp += amount
        if self.hp > self.max_hp:
            self.hp = self.max_hp


def monster_death(monster):
    """Function to call when a `monster` dies

    Args:
        monster: The monster that has died"""
    ui.message.add(monster.name.capitalize() + ' is dead! You gain ' +
                   str(monster.fighter.xp) + ' experience points.', libtcod.orange)
    monster.char = '%'
    monster.color = libtcod.dark_red
    monster.blocks = False
    monster.fighter = None
    monster.ai = None
    monster.name = 'remains of ' + monster.name
    monster.send_to_back()
