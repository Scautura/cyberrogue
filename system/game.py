import shelve

import ai
import libtcodpy as libtcod
import system
import ui


def save():
    """Save the game"""
    file = shelve.open('savegame', 'n')
    file['map'] = system.map
    file['objects'] = system.objects
    file['player_index'] = system.objects.index(system.player)
    file['inventory'] = system.inventory
    file['game_msgs'] = ui.game_msgs
    file['game_state'] = system.game_state
    file['stairs_index'] = system.objects.index(system.stairs)
    file['dungeon_level'] = system.dungeon_level
    file.close


def load():
    """Load the game"""
    file = shelve.open('savegame', 'r')
    system.map = file['map']
    system.objects = file['objects']
    system.player = system.objects[file['player_index']]
    system.inventory = file['inventory']
    ui.game_msgs = file['game_msgs']
    system.game_state = file['game_state']
    system.stairs = system.objects[file['stairs_index']]
    system.dungeon_level = file['dungeon_level']
    file.close()

    system.initialize_fov()


def handle_keys():
    """Handle input.

    Returns:
        string: A string containing current state based on input (e.g. "exit", "didnt-take-turn" or nothing if moved)."""
    if system.key.vk == libtcod.KEY_ENTER and system.key.lalt:
        # Alt+Enter: toggle fullscreen
        libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())

    elif system.key.vk == libtcod.KEY_ESCAPE:
        return 'exit'

    if system.game_state == 'playing':
        if system.key.vk == libtcod.KEY_UP or system.key.vk == libtcod.KEY_KP8:
            system.game.player_move_or_attack(0, -1)
        elif system.key.vk == libtcod.KEY_DOWN or system.key.vk == libtcod.KEY_KP2:
            system.game.player_move_or_attack(0, 1)
        elif system.key.vk == libtcod.KEY_LEFT or system.key.vk == libtcod.KEY_KP4:
            system.game.player_move_or_attack(-1, 0)
        elif system.key.vk == libtcod.KEY_RIGHT or system.key.vk == libtcod.KEY_KP6:
            system.game.player_move_or_attack(1, 0)
        elif system.key.vk == libtcod.KEY_HOME or system.key.vk == libtcod.KEY_KP7:
            system.game.player_move_or_attack(-1, -1)
        elif system.key.vk == libtcod.KEY_PAGEUP or system.key.vk == libtcod.KEY_KP9:
            system.game.player_move_or_attack(1, -1)
        elif system.key.vk == libtcod.KEY_END or system.key.vk == libtcod.KEY_KP1:
            system.game.player_move_or_attack(-1, 1)
        elif system.key.vk == libtcod.KEY_PAGEDOWN or system.key.vk == libtcod.KEY_KP3:
            system.game.player_move_or_attack(1, 1)
        elif system.key.vk == libtcod.KEY_KP5:
            pass  # do nothing ie wait for the monster to come to you

        else:
            key_char = chr(system.key.c)

            if key_char == 'g':
                for object in system.objects:
                    if object.x == system.player.x and object.y == system.player.y and object.item:
                        object.item.pick_up()
                        break
            if key_char == 'i':
                chosen_item = ui.menu.inventory(
                    'Press the key next to an item to use it, or any other to cancel.\n')
                if chosen_item is not None:
                    chosen_item.use()

            if key_char == 'd':
                chosen_item = ui.menu.inventory(
                    'Press the key next to an item to drop it, or any other to cancel.\n')
                if chosen_item is not None:
                    chosen_item.drop()

            if(key_char == ',' and system.key.shift) or key_char == '<':
                if system.stairs.x == system.player.x and system.stairs.y == system.player.y:
                    system.game.next_level()

            if key_char == 'c':
                level_up_xp = system.LEVEL_UP_BASE + system.player.level * system.LEVEL_UP_FACTOR
                ui.menu.msgbox('Character Information\n\nLevel: ' + str(system.player.level) + '\nExperience: ' + str(system.player.fighter.xp) +
                               '\nExperience to level up: ' + str(level_up_xp) + '\n\nMaximum HP: ' + str(system.player.fighter.max_hp) +
                               '\nAttack: ' + str(system.player.fighter.power) + '\nDefense: ' + str(system.player.fighter.defense), ui.CHARACTER_SCREEN_WIDTH)

            return 'didnt-take-turn'


def player_move_or_attack(dx, dy):
    """Decide whether the player moves to a new space or attacks a monster in that space.

    Args:
        dx (int): Movement along X axis
        dy (int): Movement along Y axis"""
    x = system.player.x + dx
    y = system.player.y + dy

    target = None
    for object in system.objects:
        if object.fighter and object.x == x and object.y == y:
            target = object
            break

    if target is not None:
        system.player.fighter.attack(target)

    else:
        system.player.move(dx, dy)
        system.fov_recompute = True


def player_death(player):
    """Handle the player's death.

    Args:
        player: The player object"""
    ui.message.add('You died!')
    system.game_state = 'dead'

    player.char = '%'
    player.color = libtcod.dark_red


def new_game():
    """Start a new game and initialise all required items"""
    fighter_component = ai.Fighter(
        hp=100, defense=1, power=4, xp=0, death_function=system.game.player_death)
    system.player = system.cartography.Object(0, 0, '@', 'player', libtcod.white,
                                              blocks=True, fighter=fighter_component)

    system.player.level = 1

    system.dungeon_level = 1
    system.cartography.make()

    system.initialize_fov()

    system.game_state = 'playing'
    system.inventory = []

    ui.game_msgs = []

    equipment_component = system.cartography.Equipment(
        slot='right hand', power_bonus=2)
    obj = system.cartography.Object(0, 0, '-', 'dagger', libtcod.sky,
                                    equipment=equipment_component)
    system.inventory.append(obj)
    equipment_component.equip()
    obj.always_visible = True

    ui.message.add(
        'Welcome stranger! Prepare to perish in the Tombs of the Ancient Kings', libtcod.red)


def next_level():
    """Move the player to the next level of the dungeon."""
    ui.message.add(
        'You take a moment to rest, and recover your strength.', libtcod.light_violet)
    system.player.fighter.heal(system.player.fighter.max_hp / 2)

    ui.message.add(
        'After a rare moment of peace, you descend deeper into the heart of the dungeon...', libtcod.red)
    system.dungeon_level += 1
    system.cartography.make()
    system.initialize_fov()


def check_level_up():
    """Check to see if the player has levelled up."""
    level_up_xp = system.LEVEL_UP_BASE + system.player.level * system.LEVEL_UP_FACTOR
    if system.player.fighter.xp >= level_up_xp:
        system.player.level += 1
        system.player.fighter.xp -= level_up_xp
        ui.message.add('Your battle skills grow stronger! You reached level ' +
                       str(system.player.level) + '!', libtcod.yellow)

        choice = None
        while choice == None:
            choice = ui.menu.menu('Level up! Choose a stat to raise:\n',
                                  ['Constitution (+20 HP, from ' + str(system.player.fighter.base_max_hp) + ')',
                                   'Strength (+1 attack, from ' +
                                   str(system.player.fighter.base_power) + ')',
                                   'Agility (+1 defense, from ' + str(system.player.fighter.base_defense) + ')'], system.LEVEL_UP_SCREEN_WIDTH)

            if choice == 0:
                system.player.fighter.base_max_hp += 20
                system.player.fighter.hp += 20
            elif choice == 1:
                system.player.fighter.base_power += 1
            elif choice == 2:
                system.player.fighter.base_defense += 1


def play_game():
    """Run the main game logic"""
    player_action = None

    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(
            libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, system.key, system.mouse)
        ui.render_all()
        libtcod.console_flush()
        system.game.check_level_up()
        for object in system.objects:
            object.clear()

        player_action = system.game.handle_keys()

        if player_action == 'exit':
            system.game.save()
            break

        if system.game_state == 'playing' and player_action != 'didnt-take-turn':
            for object in system.objects:
                if object.ai:
                    object.ai.take_turn()


def main_menu():
    """Display the menu when starting the game"""
    img = libtcod.image_load('menu_background.png')

    while not libtcod.console_is_window_closed():
        libtcod.image_blit_2x(img, 0, 0, 0)

        libtcod.console_set_default_foreground(0, libtcod.light_yellow)
        libtcod.console_print_ex(0, ui.SCREEN_WIDTH / 2, ui.SCREEN_HEIGHT /
                                 2 - 4, libtcod.BKGND_NONE, libtcod.CENTER, 'TOMBS OF THE ANCIENT KINGS')
        libtcod.console_print_ex(0, ui.SCREEN_WIDTH / 2, ui.SCREEN_HEIGHT -
                                 2, libtcod.BKGND_NONE, libtcod.CENTER, 'By Jotaf')

        choice = ui.menu.menu(
            '', ['Play a new game', 'Continue last game', 'Quit'], 24)

        if choice == 0:
            system.game.new_game()
            system.game.play_game()
        elif choice == 1:
            try:
                system.game.load()
            except:
                ui.menu.msgbox('\n No saved game to load.\n', 24)
                continue
            system.game.play_game()
        elif choice == 2:
            break
