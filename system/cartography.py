import math

import ai
import effects
import libtcodpy as libtcod
import system
import ui

ROOM_MAX_SIZE = 10
ROOM_MIN_SIZE = 6
MAX_ROOMS = 30


class Tile:
    """Basic tile for map

    Attributes:
        explored (bool): Whether the tile has been explored or not (defaults to `False`)
        blocked (bool): Whether the tile blocks movement
        block_sight (bool): Whether the tile blocks movement (defaults to the same as :py:attr:`~system.cartography.Tile.blocked`)

    Args:
        blocked (bool): See :py:attr:`~system.cartography.Tile.blocked`
        block_sight (bool): See :py:attr:`~system.cartography.Tile.block_sight`
    """

    def __init__(self, blocked, block_sight=None):
        self.blocked = blocked

        if block_sight is None:
            block_sight = blocked

        self.block_sight = block_sight

        self.explored = False


class Object:

    def __init__(self, x, y, char, name, color, blocks=False, always_visible=False, fighter=None, ai=None, item=None, equipment=None):
        self.name = name
        self.blocks = blocks
        self.always_visible = always_visible
        self.x = x
        self.y = y
        self.char = char
        self.color = color
        self.fighter = fighter
        self.equipment = equipment

        if self.fighter:
            self.fighter.owner = self

        self.ai = ai
        if self.ai:
            self.ai.owner = self

        self.item = item
        if self.item:
            self.item.owner = self

        if self.equipment:
            self.equipment.owner = self
            self.item = Item()
            self.item.owner = self

    def move(self, dx, dy):
        if not system.cartography.is_blocked(self.x + dx, self.y + dy):
            self.x += dx
            self.y += dy

    def move_astar(self, target):
        fov = libtcod.map_new(ui.MAP_WIDTH, ui.MAP_HEIGHT)

        for y1 in range(ui.MAP_HEIGHT):
            for x1 in range(ui.MAP_WIDTH):
                libtcod.map_set_properties(fov, x1, y1, not system.map[x1][
                                           y1].block_sight, not system.map[x1][y1].blocked)

        for obj in system.objects:
            if obj.blocks and obj != self and obj != target:
                libtcod.map_set_properties(fov, obj.x, obj.y, True, False)

        my_path = libtcod.path_new_using_map(fov, 1.41)

        libtcod.path_compute(my_path, self.x, self.y, target.x, target.y)

        if not libtcod.path_is_empty(my_path) and libtcod.path_size(my_path) < 25:
            x, y = libtcod.path_walk(my_path, True)
            if x or y:
                self.x = x
                self.y = y

        else:
            self.move_towards(target.x, target.y)

        libtcod.path_delete(my_path)

    def draw(self):
        if libtcod.map_is_in_fov(system.fov_map, self.x, self.y) or (self.always_visible and system.map[self.x][self.y].explored):
            libtcod.console_set_default_foreground(ui.con, self.color)
            libtcod.console_put_char(
                ui.con, self.x, self.y, self.char, libtcod.BKGND_NONE)

    def clear(self):
        libtcod.console_put_char(
            ui.con, self.x, self.y, ' ', libtcod.BKGND_NONE)

    def move_towards(self, target_x, target_y):
        dx = target_x - self.x
        dy = target_y - self.y
        distance = math.sqrt(dx**2 + dy**2)

        dx = int(round(dx / distance))
        dy = int(round(dy / distance))
        self.move(dx, dy)

    def distance_to(self, other):
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt(dx**2 + dy**2)

    def distance(self, x, y):
        return math.sqrt((x - self.x)**2 + (y - self.y)**2)

    def send_to_back(self):
        system.objects.remove(self)
        system.objects.insert(0, self)


class Item:

    def __init__(self, use_function=None):
        self.use_function = use_function

    def use(self):
        if self.owner.equipment:
            self.owner.equipment.toggle_equip()
            return
        if self.use_function is None:
            ui.message.add('The ' + self.owner.name + ' cannot be used.')
        else:
            if self.use_function() != 'cancelled':
                system.inventory.remove(self.owner)

    def pick_up(self):
        if len(system.inventory) >= 26:
            ui.message.add('Your inventory is full, cannot pick up ' +
                           self.owner.name + '.', libtcod.red)
        else:
            system.inventory.append(self.owner)
            system.objects.remove(self.owner)
            ui.message.add('You picked up a ' +
                           self.owner.name + '!', libtcod.green)

        equipment = self.owner.equipment
        if equipment and system.get_equipped_in_slot(equipment.slot) is None:
            equipment.equip()

    def drop(self):
        system.objects.append(self.owner)
        system.inventory.remove(self.owner)
        self.owner.x = system.player.x
        self.owner.y = system.player.y
        if self.owner.equipment:
            self.owner.equipment.dequip()
        ui.message.add('You dropped a ' +
                       self.owner.name + '.', libtcod.yellow)


class Equipment:

    def __init__(self, slot, power_bonus=0, defense_bonus=0, max_hp_bonus=0):
        self.slot = slot
        self.is_equipped = False
        self.power_bonus = power_bonus
        self.defense_bonus = defense_bonus
        self.max_hp_bonus = max_hp_bonus

    def toggle_equip(self):
        if self.is_equipped:
            self.dequip()
        else:
            self.equip()

    def equip(self):
        old_equipment = system.get_equipped_in_slot(self.slot)
        if old_equipment is not None:
            old_equipment.dequip()
        self.is_equipped = True
        ui.message.add('Equipped ' + self.owner.name + ' on ' +
                       self.slot + '.', libtcod.light_green)

    def dequip(self):
        if not self.is_equipped:
            return
        self.is_equipped = False
        ui.message.add('Dequipped ' + self.owner.name +
                       ' from ' + self.slot + '.', libtcod.light_yellow)


class Rect:
    """A rectangle with specified coordinates

    Args:
        x (int): X coordinate for top left corner
        y (int): Y coordinate for top left corner
        w (int): Width of rectangle
        h (int): Height of rectangle"""

    def __init__(self, x, y, w, h):
        self.x1 = x
        self.y1 = y
        self.x2 = x + w
        self.y2 = y + h

    def center(self):
        """Find the centre of the rectangle

        Returns:
            tuple: X, Y coordinate for the centre of the room"""
        center_x = (self.x1 + self.x2) / 2
        center_y = (self.y1 + self.y2) / 2
        return (center_x, center_y)

    def intersect(self, other):
        """Whether this room intersects with another room

        Args:
            other (system.cartography.Rect): The other room to check

        Returns:
            bool: Whether this room intersects with the given room"""
        return (self.x1 <= other.x2 and self.x2 >= other.x1 and self.y1 <= other.y2 and self.y2 >= other.y1)


def make():
    """Generate a map for a level"""
    system.objects = [system.player]

    system.map = [[system.cartography.Tile(True)
                   for y in range(ui.MAP_HEIGHT)]
                  for x in range(ui.MAP_WIDTH)]

    rooms = []
    num_rooms = 0

    for r in range(MAX_ROOMS):
        w = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
        h = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
        x = libtcod.random_get_int(0, 0, ui.MAP_WIDTH - w - 1)
        y = libtcod.random_get_int(0, 0, ui.MAP_HEIGHT - h - 1)

        new_room = Rect(x, y, w, h)

        failed = False
        for other_room in rooms:
            if new_room.intersect(other_room):
                failed = True
                break

        if not failed:
            create_room(new_room)

            (new_x, new_y) = new_room.center()

            if num_rooms == 0:
                system.player.x = new_x
                system.player.y = new_y

            else:
                (prev_x, prev_y) = rooms[num_rooms - 1].center()

                if libtcod.random_get_int(0, 0, 1) == 1:
                    create_h_tunnel(prev_x, new_x, prev_y)
                    create_v_tunnel(prev_y, new_y, new_x)
                else:
                    create_v_tunnel(prev_y, new_y, prev_x)
                    create_h_tunnel(prev_x, new_x, new_y)

            place_objects(new_room)
            rooms.append(new_room)
            num_rooms += 1

    system.stairs = Object(new_x, new_y, '<', 'stairs',
                           libtcod.white, always_visible=True)
    system.objects.append(system.stairs)
    system.stairs.send_to_back()


def place_objects(room):
    """Fill an empty room with objects and monsters

    Args:
        room (system.cartography.Rect): A room with X/Y coordinates (`x1`, `y1`, `x2`, `y2`) specifying the corners of the room"""
    max_monsters = system.from_dungeon_level([[2, 1], [3, 4], [5, 6]])

    monster_chances = {}
    monster_chances['orc'] = 80
    monster_chances['troll'] = system.from_dungeon_level(
        [[15, 3], [30, 5], [60, 7]])

    max_items = system.from_dungeon_level([[1, 1], [2, 4]])

    item_chances = {}
    item_chances['heal'] = 35
    item_chances['lightning'] = system.from_dungeon_level([[25, 4]])
    item_chances['fireball'] = system.from_dungeon_level([[25, 6]])
    item_chances['confuse'] = system.from_dungeon_level([[10, 2]])
    item_chances['sword'] = system.from_dungeon_level([[5, 4]])
    item_chances['shield'] = system.from_dungeon_level([[15, 8]])

    num_monsters = libtcod.random_get_int(0, 0, max_monsters)

    for i in range(num_monsters):
        x = libtcod.random_get_int(0, room.x1 + 1, room.x2 - 1)
        y = libtcod.random_get_int(0, room.y1 + 1, room.y2 - 1)

        if not is_blocked(x, y):
            choice = system.random_choice(monster_chances)
            if choice == 'orc':
                fighter_component = ai.Fighter(
                    hp=20, defense=0, power=4, xp=35, death_function=ai.monster_death)
                ai_component = ai.monster.Basic()

                monster = Object(x, y, 'o', 'orc', libtcod.desaturated_green,
                                 blocks=True, fighter=fighter_component, ai=ai_component)
            else:
                fighter_component = ai.Fighter(
                    hp=30, defense=2, power=8, xp=100, death_function=ai.monster_death)
                ai_component = ai.monster.Basic()

                monster = Object(x, y, 'T', 'troll', libtcod.darker_green,
                                 blocks=True, fighter=fighter_component, ai=ai_component)

            system.objects.append(monster)

    num_items = libtcod.random_get_int(0, 0, max_items)

    for i in range(num_items):
        x = libtcod.random_get_int(0, room.x1 + 1, room.x2 - 1)
        y = libtcod.random_get_int(0, room.y1 + 1, room.y2 - 1)

        if not is_blocked(x, y):
            choice = system.random_choice(item_chances)
            if choice == 'heal':
                item_component = Item(use_function=effects.magic.heal)
                item = Object(x, y, '!', 'healing potion', libtcod.violet,
                              item=item_component, always_visible=True)
            elif choice == 'lightning':
                item_component = Item(use_function=effects.magic.lightning)
                item = Object(x, y, '#', 'scroll of lightning bolt',
                              libtcod.light_yellow, item=item_component, always_visible=True)
            elif choice == 'fireball':
                item_component = Item(use_function=effects.magic.fireball)
                item = Object(x, y, '#', 'scroll of fireball',
                              libtcod.light_yellow, item=item_component, always_visible=True)
            elif choice == 'confuse':
                item_component = Item(use_function=effects.magic.confuse)
                item = Object(x, y, '#', 'scroll of confusion',
                              libtcod.light_yellow, item=item_component, always_visible=True)
            elif choice == 'sword':
                equipment_component = Equipment(
                    slot='right hand', power_bonus=3)
                item = Object(x, y, '/', 'sword', libtcod.sky,
                              equipment=equipment_component)
            elif choice == 'shield':
                equipment_component = Equipment(
                    slot='left hand', defense_bonus=1)
                item = Object(
                    x, y, '[', 'shield', libtcod.darker_orange, equipment=equipment_component)

            system.objects.append(item)
            item.send_to_back()


def create_room(room):
    """Create an empty room with given coordinates

    Args:
        room (system.cartography.Rect): A room with X/Y coordinates (`x1`, `y1`, `x2`, `y2`) specifying the corners of the room"""
    for x in range(room.x1 + 1, room.x2):
        for y in range(room.y1 + 1, room.y2):
            system.map[x][y].blocked = False
            system.map[x][y].block_sight = False


def create_h_tunnel(x1, x2, y):
    """Create a horizontal tunnel with the given coordinates

    Args:
        x1 (int): Left/right X coordinate of tunnel
        x2 (int): Left/right X coordinate of tunnel
        y (int): Y coordinate of tunnel"""
    for x in range(min(x1, x2), max(x1, x2) + 1):
        system.map[x][y].blocked = False
        system.map[x][y].block_sight = False


def create_v_tunnel(y1, y2, x):
    """Create a vertical tunnel with the given coordinates

    Args:
        y1 (int): Top/bottom Y coordinate of tunnel
        y2 (int): Top/bottom Y coordinate of tunnel
        x (int): X coordinate of tunnel"""
    for y in range(min(y1, y2), max(y1, y2) + 1):
        system.map[x][y].blocked = False
        system.map[x][y].block_sight = False


def is_blocked(x, y):
    """Whether a specified tile is blocked

    Args:
        x (int): X coordinate
        y (int): Y coordinate

    Returns:
        bool: Whether the specified tile is blocked (either inherently, or by an object that is on it)"""
    if system.map[x][y].blocked:
        return True

    for object in system.objects:
        if object.blocks and object.x == x and object.y == y:
            return True

    return False
