import libtcodpy as libtcod
import system.cartography
import system.game
import ui

LEVEL_UP_BASE = 200
LEVEL_UP_FACTOR = 150
LEVEL_UP_SCREEN_WIDTH = 40

LIMIT_FPS = 20

FOV_ALGO = 0
FOV_LIGHT_WALLS = True
TORCH_RADIUS = 10

libtcod.console_set_custom_font(
    'data/fonts/dejavu10x10_gs_tc.png', libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD)

libtcod.console_init_root(ui.SCREEN_WIDTH, ui.SCREEN_HEIGHT,
                          'python/libtcod tutorial', False)

libtcod.sys_set_fps(LIMIT_FPS)

mouse = libtcod.Mouse()
key = libtcod.Key()


def random_choice_index(chances):
    """Choose one option from a list of chances and return the index

    Args:
        chances (list): A list of chances of items

    Returns:
        int: Index of the chosen chance
    """
    dice = libtcod.random_get_int(0, 1, sum(chances))

    running_sum = 0
    choice = 0
    for w in chances:
        running_sum += w

        if dice <= running_sum:
            return choice
        choice += 1


def random_choice(chances_dict):
    """Choose one option from a dictionary of chances and return its key

    Pass a dictionary in the following form::

    ['item1', 10, 'item2', 20, 'item3', 30, ...]

    Args:
        chances_dict (dict): A dictionary of strings and chances each string will happen

    Returns:
        str: The string that was chosen using :py:func:`~system.random_choice_index`
    """
    chances = chances_dict.values()
    strings = chances_dict.keys()

    return strings[random_choice_index(chances)]


def from_dungeon_level(table):
    """Returns a value that depends on level

    The table specifies what value occurs after each level, defaults to 0

    Args:
        table (dict): List of values occurring at and higher than a specific level

    Returns:
        Value occurring at current `dungeon_level`
    """
    for(value, level) in reversed(table):
        if system.dungeon_level >= level:
            return value
    return 0


def closest_monster(max_range):
    """Find closest enemy, up to `max_range` from the player, and in FOV, or `None`

    Args:
        max_range (int): Maximum distance from player to check for max_monsters

    Returns:
        The closest enemy, or `None`
    """
    closest_enemy = None
    closest_dist = max_range + 1
    for object in objects:
        if object.fighter and not object == system.player and libtcod.map_is_in_fov(system.fov_map, object.x, object.y):
            dist = system.player.distance_to(object)
            if dist < closest_dist:
                closest_enemy = object
                closest_dist = dist

    return closest_enemy


def target_monster(max_range=None):
    """Returns a clicked monster inside FOV and up to `max_range` from the player, or `None` if right-clicked

    Args:
        max_range (int): Maximum range from player that a monster can be selected

    Returns:
        Monster that is clicked on, or `None`
    """

    while True:
        (x, y) = system.target_tile(max_range)

        if x is None:
            return None

        for obj in objects:
            if obj.x == x and obj.y == y and obj.fighter and obj != system.player:
                return obj


def target_tile(max_range=None):
    """Returns the position of a tile that is left clicked within a player's FOV (and optionally a `max_range`) or `(None, None)` if right-clicked

    Args:
        max_range (int): Maximum range from player that a tile can be selected

    Returns:
        X and Y values of selected tile, or `(None, None)`
    """
    while True:
        libtcod.console_flush()
        libtcod.sys_check_for_event(
            libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, system.key, system.mouse)
        ui.render_all()

        (x, y) = (system.mouse.cx, system.mouse.cy)

        if(system.mouse.lbutton_pressed and libtcod.map_is_in_fov(system.fov_map, x, y) and (max_range is None or system.player.distance(x, y) <= max_range)):
            return (x, y)
        if system.mouse.rbutton_pressed or system.key.vk == libtcod.KEY_ESCAPE:
            return (None, None)


def get_names_under_mouse():
    """Return a string with the names of all objects under the mouse

    Returns:
        (str): A capitalized string of all the objects under the mouse, separated by commas
    """
    (x, y) = (system.mouse.cx, system.mouse.cy)

    names = [obj.name for obj in system.objects
             if obj.x == x and obj.y == y and libtcod.map_is_in_fov(system.fov_map, obj.x, obj.y)]

    names = ', '.join(names)
    return names.capitalize()


def initialize_fov():
    """Initialise the field of view"""
    system.fov_recompute = True

    system.fov_map = libtcod.map_new(ui.MAP_WIDTH, ui.MAP_HEIGHT)
    for y in range(ui.MAP_HEIGHT):
        for x in range(ui.MAP_WIDTH):
            libtcod.map_set_properties(system.fov_map, x, y, not system.map[x][
                                       y].block_sight, not system.map[x][y].blocked)

    libtcod.console_clear(ui.con)


def get_equipped_in_slot(slot):
    """Get an item equipped in a specific slot, otherwise `None`

    Args:
        slot (str): String representing a specific slot

    Returns:
        The object in the specified slot, or `None`"""
    for obj in system.inventory:
        if obj.equipment and obj.equipment.slot == slot and obj.equipment.is_equipped:
            return obj.equipment
    return None


def get_all_equipped(obj):
    """Get all items that are equipped by `obj`

    Args:
        obj: An object that can equip items (currently only checks for player)

    Returns:
        list: All objects equipped by `obj` (only checks for player, returns an empty list for everything else)"""
    if obj == system.player:
        equipped_list = []
        for item in system.inventory:
            if item.equipment and item.equipment.is_equipped:
                equipped_list.append(item.equipment)
        return equipped_list
    else:
        return []
