import ai
import effects
import libtcodpy as libtcod
import system
import ui

HEAL_AMOUNT = 40
LIGHTNING_RANGE = 5
LIGHTNING_DAMAGE = 40

FIREBALL_RADIUS = 3
FIREBALL_DAMAGE = 25

CONFUSE_NUM_TURNS = 10
CONFUSE_RANGE = 8


def heal():
    """Heal the player

    Returns:
        (str): `cancelled` if the player is already at full health
    """
    if system.player.fighter.hp == system.player.fighter.max_hp:
        ui.message.add('You are already at full health.', libtcod.red)
        return 'cancelled'

    ui.message.add('Your wounds start to feel better!', libtcod.light_violet)
    system.player.fighter.heal(HEAL_AMOUNT)


def lightning():
    """Find the closest enemy (within a maximum range) and damage it

    Returns:
        (str): `cancelled` if no monster is within range
    """
    monster = system.closest_monster(LIGHTNING_RANGE)
    if monster is None:
        ui.message.add('No enemy is close enough to strike.', libtcod.red)
        return 'cancelled'

    ui.message.add('A lightning bolt strikes the ' + monster.name +
                   ' with a loud thunder! The damage is ' + str(LIGHTNING_DAMAGE) + ' hit points.', libtcod.light_blue)
    monster.fighter.take_damage(LIGHTNING_DAMAGE)


def confuse():
    """Ask the player for a monster to confuse

    Returns:
        `cancelled` if player right-clicks
    """
    ui.message.add(
        'Left-click an enemy to confuse it, or right-click to cancel.', libtcod.light_cyan)
    monster = system.target_monster(CONFUSE_RANGE)
    if monster is None:
        return 'cancelled'

    old_ai = monster.ai
    monster.ai = ai.monster.Confused(old_ai)
    monster.ai.owner = monster
    ui.message.add('The eyes of the ' + monster.name +
                   ' look vacant, as he starts to stumble around!', libtcod.light_green)


def fireball():
    """Ask player for a tile to cast fireball at

    Returns:
        `cancelled` if player right clicks"""
    ui.message.add(
        'Left-click a target tile for the fireball, or right-click to cancel.', libtcod.cyan)
    (x, y) = system.target_tile()

    if x is None:
        return 'cancelled'
    ui.message.add('The fireball explodes, burning everything within ' +
                   str(FIREBALL_RADIUS) + ' tiles!', libtcod.orange)

    for obj in system.objects:
        if obj.distance(x, y) <= FIREBALL_RADIUS and obj.fighter:
            ui.message.add('The ' + obj.name + ' gets burned for ' +
                           str(FIREBALL_DAMAGE) + ' hit points.', libtcod.orange)
            obj.fighter.take_damage(FIREBALL_DAMAGE)
