Effects
=======

.. automodule:: effects
   :members:
   :undoc-members:

.. automodule:: effects.magic
   :members:
   :undoc-members:
